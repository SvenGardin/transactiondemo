/**
 *
 */
package com.cgi.osd.transactiondemo.logic.domainobject;

import java.util.HashSet;
import java.util.Set;

/**
 * This class is responsible for implementing the domain object Customer. All properties but telephone number is mandatory.
 *
 *
 * @author anderssonhr
 *
 */
public class CustomerDO {

    private String firstName;
    private String lastName;
    private final String email;
    private String telephoneNumber;
    private Set<String> bookingReferences = new HashSet<String>();

    /**
     * Constructs an instance with email only. Note that the email address is considered to identify the customer.
     *
     * @param email
     *            customer's email address.
     */
    public CustomerDO(String email) {
	super();
	this.email = email;
    }

    /**
     * Constructs an instance with some properties.
     *
     * @param firstName
     *            customer's first name
     * @param lastName
     *            customer's last name.
     * @param email
     *            customer's email address.
     */
    public CustomerDO(String firstName, String lastName, String email) {
	super();
	this.firstName = firstName;
	this.lastName = lastName;
	this.email = email;
    }

    /**
     * Constructs an instance with all properties but bookings.
     *
     * @param firstName
     *            customer's first name
     * @param lastName
     *            customer's last name.
     * @param email
     *            customer's email address.
     * @param telphoneNumber
     *            customer's telephone number.
     */
    public CustomerDO(String firstName, String lastName, String email, String telephoneNumber) {
	super();
	this.firstName = firstName;
	this.lastName = lastName;
	this.email = email;
	this.telephoneNumber = telephoneNumber;
    }

    /**
     * Constructs an instance with all properties.
     *
     * @param firstName
     *            customer's first name
     * @param lastName
     *            customer's last name.
     * @param email
     *            customer's email address.
     * @param telphoneNumber
     *            customer's telephone number.
     * @param bookingReferences
     *            a set of booking references.
     */
    public CustomerDO(String firstName, String lastName, String email, String telephoneNumber, Set<String> bookingReferences) {
	super();
	this.firstName = firstName;
	this.lastName = lastName;
	this.email = email;
	this.telephoneNumber = telephoneNumber;
	this.bookingReferences = bookingReferences;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	final CustomerDO other = (CustomerDO) obj;
	if (this.email == null) {
	    if (other.email != null) {
		return false;
	    }
	} else if (!this.email.equals(other.email)) {
	    return false;
	}
	return true;
    }

    public Set<String> getBookingReferences() {
	return this.bookingReferences;
    }

    public String getEmail() {
	return this.email;
    }

    public String getFirstName() {
	return this.firstName;
    }

    public String getLastName() {
	return this.lastName;
    }

    public String getTelephoneNumber() {
	return this.telephoneNumber;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + (this.email == null ? 0 : this.email.hashCode());
	return result;
    }

    public boolean isMandatorySet() {
	return this.email != null && !this.email.isEmpty() && this.firstName != null && !this.firstName.isEmpty() && this.lastName != null
		&& !this.lastName.isEmpty();

    }

    public void setBookingReferences(Set<String> bookingReferences) {
	this.bookingReferences = bookingReferences;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public void setTelephoneNumber(String telephoneNumber) {
	this.telephoneNumber = telephoneNumber;
    }

}

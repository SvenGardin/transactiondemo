package com.cgi.osd.transactiondemo.logic.domainobject;

/**
 * This enum defines the possible lock strategies.
 * 
 * @author anderssonhr
 *
 */
public enum LockStrategy {
    OPTIMISTIC, PESSIMISTIC

}

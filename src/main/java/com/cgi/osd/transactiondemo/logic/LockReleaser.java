package com.cgi.osd.transactiondemo.logic;

import java.util.logging.Logger;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;

import com.cgi.osd.transactiondemo.logic.exception.SeatBookedException;
import com.cgi.osd.transactiondemo.persistence.PersistenceManager;

/**
 * This class is responsible for performing a scheduled removal of old seat locks in the database.
 *
 */
@Singleton
public class LockReleaser {

    @Inject
    private Logger logger;
    @Inject
    private PersistenceManager persistenceFacade;
    @Inject
    BookingService bookingService;

    @Schedule(hour = "*", minute = "*", second = "0/30", persistent = false)
    public void releaseOldSeatLocks() {
	try {
	    final int updated = this.persistenceFacade.releaseOldSeatLocks(5);
	    if (updated > 0) {
		this.bookingService.sendNotification(updated);
	    }
	} catch (final SeatBookedException e) {
	    this.logger.warning("Failed to release old seat locks.");
	}
    }

}

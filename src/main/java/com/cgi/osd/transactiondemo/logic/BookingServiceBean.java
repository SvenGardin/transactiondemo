package com.cgi.osd.transactiondemo.logic;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.logging.Logger;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.LockStrategy;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.SeatBookedException;
import com.cgi.osd.transactiondemo.persistence.PersistenceManager;

/**
 * This class is responsible for implementing the booking services, which means implementing the main business logic for
 * the system.
 *
 */
@Singleton
@TransactionAttribute(TransactionAttributeType.NEVER)
public class BookingServiceBean extends Observable implements BookingService {

	@Inject
	private Logger logger;
	@Inject
	private PersistenceManager persistenceManager;

	@Override
	public void cancelBookings(Collection<SeatDO> seats) {
		throw new RuntimeException("Not yet implemented.");
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void createBookings(CustomerDO customer, Collection<SeatDO> seats) throws SeatBookedException {
		final Set<SeatDO> bookedSeats = new HashSet<>();
		for (final SeatDO seat : seats) {
			if (seat != null && !seat.isBooked()) {
				bookedSeats.add(seat);
			}
		}
		if (!bookedSeats.isEmpty()) {
			this.persistenceManager.createBookings(customer, bookedSeats);
		}
		sendNotification(seats.size());
	}

	@Override
	public List<SeatDO> getAllSeats() {
		throw new RuntimeException("Not yet implemented.");
	}

	@Override
	public List<SeatDO> getBookedSeats() {
		throw new RuntimeException("Not yet implemented.");
	}

	@Override
	public CustomerDO getCustomerFromEmail(String email) {
		final CustomerDO customer = this.persistenceManager.getCustomer(email);
		return customer;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Override
	public List<SeatDO> getCustomersAndFreeSeat(CustomerDO customer) {
		List<SeatDO> seats;
		if (customer == null || getCustomerFromEmail(customer.getEmail()) == null) {
			seats = this.persistenceManager.getFreeSeats();
		} else {
			seats = this.persistenceManager.getCustomerSeats(customer);
		}
		return seats;
	}

	@Override
	public List<SeatDO> getFreeSeats() {
		final List<SeatDO> seats = this.persistenceManager.getFreeSeats();
		return seats;
	}

	@Override
	public LockStrategy getLockStrategy() {
		return this.persistenceManager.getLockStrategy();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void lockSeat(SeatDO seat, CustomerDO customer, boolean lock) throws SeatBookedException {
		this.persistenceManager.lockSeat(seat, customer, lock);
		if (getLockStrategy() == LockStrategy.PESSIMISTIC) {
			sendNotification(1);
		}
	}

	@Override
	@Lock(LockType.WRITE)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void registerObserver(Observer observer) {
		super.addObserver(observer);
		this.logger.fine("Observer registered.");
	}

	@Override
	public void releaseSeats(CustomerDO customer, Collection<SeatDO> seats) throws SeatBookedException {
		for (final SeatDO seat : seats) {
			this.persistenceManager.lockSeat(seat, customer, false);
		}
		sendNotification(seats.size());
	}

	@Override
	public void sendNotification(int numberOfSeats) {
		super.setChanged();
		super.notifyObservers(new Integer(numberOfSeats));
		super.clearChanged();
	}

	@Override
	public void setLockStrategy(LockStrategy lockStrategy) {
		this.persistenceManager.setLockStrategy(lockStrategy);
	}

	@Override
	@Lock(LockType.WRITE)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void unregisterObserver(Observer observer) {
		super.deleteObserver(observer);
		this.logger.fine("Observer unregistered.");
	}

}

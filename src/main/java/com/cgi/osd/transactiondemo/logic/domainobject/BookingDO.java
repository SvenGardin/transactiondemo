/**
 *
 */
package com.cgi.osd.transactiondemo.logic.domainobject;

import java.util.Collection;

/**
 * This class is responsible for implementing the domain object Booking. In our domain a booking can be changed but it will always belong to
 * the same customer.
 *
 * @author anderssonhr
 *
 */
public class BookingDO {

    private final String customerEmail;
    private Collection<Integer> seatNumbers;
    private final String bookingReference;

    /**
     * Constructs a customer domain object.
     *
     * @param customer
     *            the customer who owns the bookings.
     * @param seats
     *            a collection of booked seat numbers.
     * @param bookingReference
     *            an unique id for the booking.
     *
     */
    public BookingDO(String customerEmail, Collection<Integer> seatNumbers, String bookingReference) {
	super();
	this.customerEmail = customerEmail;
	this.seatNumbers = seatNumbers;
	this.bookingReference = bookingReference;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	final BookingDO other = (BookingDO) obj;
	if (this.bookingReference == null) {
	    if (other.bookingReference != null) {
		return false;
	    }
	} else if (!this.bookingReference.equals(other.bookingReference)) {
	    return false;
	}
	return true;
    }

    public String getBookingReference() {
	return this.bookingReference;
    }

    public String getCustomerEmail() {
	return this.customerEmail;
    }

    public Collection<Integer> getSeatNumbers() {
	return this.seatNumbers;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + (this.bookingReference == null ? 0 : this.bookingReference.hashCode());
	return result;
    }

    public void setSeatNumbers(Collection<Integer> seatNumbers) {
	this.seatNumbers = seatNumbers;
    }

}

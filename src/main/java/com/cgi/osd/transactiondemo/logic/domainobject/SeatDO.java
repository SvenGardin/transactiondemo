/**
 *
 */
package com.cgi.osd.transactiondemo.logic.domainobject;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * This class is responsible for implementing the domain object Seat.
 *
 * @author anderssonhr
 *
 */
public class SeatDO implements Comparable<SeatDO> {

	private int seatNumber;
	private String bookingReference = "";
	private boolean pessimisticLocked = false;

	public SeatDO() {
		// Required by Jackson in test
	}

	/**
	 * Constructs an instance of SeatDO setting the seat number field.
	 *
	 * @param seatNumber
	 *            the number of the seat.
	 */
	public SeatDO(int seatNumber) {
		this.seatNumber = seatNumber;
	}

	/**
	 * Constructs an instance of SeatDO setting all fields.
	 *
	 * @param seatNumber
	 *            the number of the seat.
	 * @param bookingReference
	 *            a reference to the booking which includes this seat.
	 */
	public SeatDO(int seatNumber, String bookingReference) {
		super();
		this.seatNumber = seatNumber;
		this.bookingReference = bookingReference;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SeatDO o) {
		if (o.getSeatNumber() > getSeatNumber()) {
			return 1;
		}
		if (o.getSeatNumber() < getSeatNumber()) {
			return -1;
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final SeatDO other = (SeatDO) obj;
		if (this.seatNumber != other.seatNumber) {
			return false;
		}
		return true;
	}

	public String getBookingReference() {
		return this.bookingReference;
	}

	public int getSeatNumber() {
		return this.seatNumber;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.seatNumber;
		return result;
	}

	@JsonIgnore
	public boolean isBooked() {
		return this.bookingReference == null || this.bookingReference.isEmpty() ? false : true;
	}

	public boolean isPessimisticLocked() {
		return this.pessimisticLocked;
	}

	public void setBookingReference(String bookingReference) {
		this.bookingReference = bookingReference;
	}

	public void setPessimisticLocked(boolean pessimisticLocked) {
		this.pessimisticLocked = pessimisticLocked;
	}

}

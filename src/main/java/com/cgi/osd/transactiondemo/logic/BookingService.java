package com.cgi.osd.transactiondemo.logic;

import java.util.Collection;
import java.util.List;
import java.util.Observer;

import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.LockStrategy;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.SeatBookedException;

/**
 * This interface defines the method required for handling bookings.
 *
 * @author anderssonhr
 *
 */
public interface BookingService {

    /**
     * This method cancels bookings. Cancellation of non-booked seats is allowed.
     *
     * @param seats
     *            collection of seats from which the booking should be removed.
     */
    public void cancelBookings(Collection<SeatDO> seats);

    /**
     * This method creates bookings. The customer is created if not existing. For best performance only booked seat
     * objects should be supplied.
     *
     * @param customer
     *            the customer represented by a domain object.
     * @param seats
     *            a list of the seat domain objects to be booked.
     * @throws SeatBookedException
     *             thrown when the booking couldn't be created. Will be thrown if the seat is already booked.
     */
    public void createBookings(CustomerDO customer, Collection<SeatDO> seats) throws SeatBookedException;

    /**
     * This method returns a list of all seats.
     *
     * @return a list of all seats.
     */
    public List<SeatDO> getAllSeats();

    /**
     * This method returns a list of all already booked seats.
     *
     * @return a list of all booked seats.
     */
    public List<SeatDO> getBookedSeats();

    /**
     * This method tries to find a customer domain object from the email.
     *
     * @param email
     *            the customer's email.
     * @return a customer domain object or null if no customer found for the given email.
     */
    public CustomerDO getCustomerFromEmail(String email);

    /**
     * This method a returns list of all free seats and the seats booked by a certain customer, if customer reference is
     * supplied.
     *
     * @param customer
     *            a customer domain object or null.
     * @return a list of all free seats and seats booked by the customer if applicable. The set may be empty.
     */
    public List<SeatDO> getCustomersAndFreeSeat(CustomerDO customer);

    /**
     * This method returns a list of all free seats.
     *
     * @return a list of all free seats. The list may be empty.
     */
    public List<SeatDO> getFreeSeats();

    /**
     * This method return the actual seat lock strategy.
     *
     * @return the actual seat lock strategy.
     */
    public LockStrategy getLockStrategy();

    /**
     * This method locks or releases one seat according to actual strategy, optimistic or pessimistic. The persistence
     * layer is responsible for releasing seats "forgotten" by the client.
     *
     * @param seat
     *            the seat to be locked/released.
     * @param customer
     *            the customer who changes the lock state.
     * @param lock
     *            true if the seat should be locked, false if released.
     * @throws SeatBookingException
     *             thrown if a seat cannot be locked. May happen is seat is already booked.
     */
    public void lockSeat(SeatDO seat, CustomerDO customer, boolean lock) throws SeatBookedException;

    /**
     * Register an instance for notification when the bookings on the seat list has changed.
     *
     * @param observer
     *            the instance that will receive notifications.
     */
    public void registerObserver(Observer observer);

    /**
     * This method releases seat locks for one customer, optimistic as well as pessimistic locks are removed.
     *
     * @param customer
     *            the customer who have locked the seats.
     * @param seats
     *            the collection of seat to be released.
     * @throws SeatBookedException
     *             if seats couldn't be released.
     */
    public void releaseSeats(CustomerDO customer, Collection<SeatDO> seats) throws SeatBookedException;

    /**
     * Send notifications to observers that the status has changed on a number of seats.
     *
     */
    public void sendNotification(int numberOfSeats);

    /**
     * This method sets the actual seat lock strategy.
     *
     * @param lockStrategy
     *            lock strategy to be set.
     */
    public void setLockStrategy(LockStrategy lockStrategy);

    /**
     * Unregister an instance for notification when the bookings on the seat list has changed.
     *
     * @param observer
     *            the instance that should be removed from the notification receiver list.
     */
    public void unregisterObserver(Observer observer);
}

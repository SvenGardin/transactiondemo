package com.cgi.osd.transactiondemo.logic.exception;

/**
 * This class is responsible for representing and holding information about an error that occurs when creating a
 * booking.
 *
 * @author anderssonhr
 *
 */
public class SeatBookedException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Constructs a BookingException object.
     *
     * @param message
     *            the reason why the booking creation failed.
     */
    public SeatBookedException(String message) {
	super(message);
    }
}

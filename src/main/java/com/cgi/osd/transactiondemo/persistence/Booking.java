package com.cgi.osd.transactiondemo.persistence;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the booking database table.
 *
 */
@Entity
@Table(name = "booking")
@NamedQueries({ @NamedQuery(name = "Booking.findAll", query = "SELECT b FROM Booking b"),
	@NamedQuery(name = "Booking.findByCustomer", query = "SELECT b FROM Booking b JOIN b.customer c   WHERE c.email = :customersEmail") })
public class Booking extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "booking_reference", unique = true, nullable = false, length = 255)
    private String bookingReference;

    // bi-directional many-to-one association to Customer
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "p_key_customer", referencedColumnName = "p_key", nullable = false)
    private Customer customer;

    // bi-directional many-to-one association to Seat
    @OneToMany(mappedBy = "booking")
    private Set<Seat> seats = new HashSet<>();

    public Booking() {
    }

    /**
     * Constructs an instance with its booking reference.
     *
     * @param bookingReference
     */
    public Booking(String bookingReference) {
	super();
	this.bookingReference = bookingReference;
    }

    public String getBookingReference() {
	return this.bookingReference;
    }

    public Customer getCustomer() {
	return this.customer;
    }

    public Set<Seat> getSeats() {
	return this.seats;
    }

    public void setBookingReference(String bookingReference) {
	this.bookingReference = bookingReference;
    }

    public void setCustomer(Customer customer) {
	customer.getBookings().add(this);
	this.customer = customer;
    }

    public void setSeats(Set<Seat> seats) {
	this.seats = seats;
    }

}
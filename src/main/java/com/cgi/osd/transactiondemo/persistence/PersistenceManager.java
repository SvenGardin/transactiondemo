package com.cgi.osd.transactiondemo.persistence;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.cgi.osd.transactiondemo.logic.domainobject.BookingDO;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.LockStrategy;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.SeatBookedException;

/**
 * This interface defines all methods that should be used for persistence operations.
 *
 */
public interface PersistenceManager {

	/**
	 * This method creates bookings. The customer is created if not existing.
	 *
	 * @param customer
	 *            the customer represented by a domain object.
	 * @param seats
	 *            a collection of the seat domain objects to be booked.
	 * @throws SeatBookedException
	 *             thrown when the booking couldn't be created. Will be thrown if the seat is already booked.
	 */
	public void createBookings(CustomerDO customerDO, Collection<SeatDO> seatDOs) throws SeatBookedException;

	/**
	 * This method return the actual seat lock strategy.
	 *
	 * @return the actual seat lock strategy.
	 */
	public LockStrategy getLockStrategy();

	/**
	 * This method locks or releases one seat. A scheduled job will release "forgotten" locked seats.
	 *
	 * @param seat
	 *            the seat to be locked/released.
	 * @param customer
	 *            the customer who changes the lock state.
	 * @param lock
	 *            true if the seat should be locked, false if released.
	 * @throws SeatBookingException
	 *             thrown if a seat cannot be locked. May happen is seat is already booked.
	 */
	public void lockSeat(SeatDO seatDO, CustomerDO customer, boolean lock) throws SeatBookedException;

	/**
	 * This method releases the seat locks where the lock time exceeds a limit. Both optimistic and pessimistic locks
	 * are removed.
	 *
	 * @param maxLockTimeMinutes
	 *            the maximum time allowed for seat locks.
	 * @throws SeatBookedException
	 *             if a seat can't be unlocked.
	 * @return the number of released pessimistic locks.
	 */
	public int releaseOldSeatLocks(int maxLockMinutes) throws SeatBookedException;

	/**
	 * This method releases all seat locks for one customer.
	 *
	 * @param customer
	 *            the customer whose locks should be removed.
	 * @throws SeatBookedException
	 *             thrown if seats couldn't be released.
	 */
	public void releaseSeats(CustomerDO customerDO) throws SeatBookedException;

	/**
	 * This method sets the actual seat lock strategy.
	 *
	 * @param lockStrategy
	 *            lock strategy to be set.
	 */
	public void setLockStrategy(LockStrategy lockStrategy);

	/**
	 * This method returns a customer domain object for a given email address.
	 *
	 * @param email
	 *            the customer's email address.
	 * @return the customer domain object or null if none was found.
	 */
	CustomerDO getCustomer(String email);

	/**
	 * This method returns a set of bookings belonging to one customer.
	 *
	 * @param customer
	 *            the actual customer represented by the domain object.
	 * @return a set of booking domain objects. The set can be empty.
	 */
	Set<BookingDO> getCustomersBookings(CustomerDO customerDO);

	/**
	 * This method returns a list of all free seats and the seats booked by the actual customer.
	 *
	 * @param customer
	 *            the actual customer, may be null.
	 * @return a list of seats. The list can be empty.
	 */
	List<SeatDO> getCustomerSeats(CustomerDO customerDO);

	/**
	 * This method returns a list of all free seats.
	 *
	 * @return a list of free seats. The list might be empty.
	 */
	List<SeatDO> getFreeSeats();

}

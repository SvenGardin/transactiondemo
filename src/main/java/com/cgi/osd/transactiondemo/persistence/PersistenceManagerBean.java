package com.cgi.osd.transactiondemo.persistence;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.OptimisticLockException;
import javax.persistence.PessimisticLockException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.cgi.osd.transactiondemo.logic.domainobject.BookingDO;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.LockStrategy;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.SeatBookedException;

/**
 * This class is responsible for implementing methods for persistence handling. All methods are thread safe.
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.NEVER)
@Interceptors(ExceptionInterceptor.class)
public class PersistenceManagerBean implements PersistenceManager {

	@Inject
	private Logger logger;
	@Inject
	private EntityManager em;
	@Inject
	private PersistenceObjectFactory objectFactory;
	@Inject
	private LockTracker lockTracker;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void createBookings(CustomerDO customerDO, Collection<SeatDO> seatDOs) throws SeatBookedException {
		final Customer customer = getAndUpdateOrCreateCustomer(customerDO, getActualLockMode());
		final String bookingReferenceText = customer.getEmail() + System.currentTimeMillis();
		final String bookingReference = createHash(bookingReferenceText);
		final Booking booking = new Booking();
		booking.setBookingReference(bookingReference);
		booking.setCustomer(customer);
		this.em.persist(booking);
		this.em.lock(booking, getActualLockMode());
		customer.getBookings().add(booking);

		final Map<Integer, Seat> seatsToBook = getSeatsFromSeatDOs(seatDOs, getActualLockMode());
		for (final SeatDO seatDO : seatDOs) {
			final Seat seatToBook = seatsToBook.get(new Integer(seatDO.getSeatNumber()));
			if (seatToBook == null) {
				continue;
			}
			/*
			 * Both optimistic and pessimistic locks must be checked manually since the locks exists over more than one
			 * code. The version field used for optimistic lock check must never be set manually in a entity object
			 * managed by the persistence context according to JPA specification.
			 *
			 * Check methods throw optimistic lock exception respective pessimistic lock exception.
			 */
			if (this.lockTracker.getLockStrategy() == LockStrategy.OPTIMISTIC) {
				checkOptimistic(customer, seatToBook);
			} else {
				checkPessimistic(customer, seatToBook);
			}
			seatToBook.setBooking(booking);
			seatToBook.setLocked(false);
			seatToBook.setLockTime(null);
			seatToBook.setLockedBy(null);
			booking.getSeats().add(seatToBook);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public CustomerDO getCustomer(String email) {
		final Customer customer = getCustomerFromEmail(email, getActualLockMode());
		if (customer == null) {
			return null;
		}
		final CustomerDO customerDO = this.objectFactory.createCustomerDO(customer);
		return customerDO;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Set<BookingDO> getCustomersBookings(CustomerDO customer) {
		Set<BookingDO> result = new HashSet<>();
		if (customer == null) {
			this.logger.warning("doGetCustomersBookings invoked with null parameter");
			return result;
		}
		final TypedQuery<Booking> query = this.em.createNamedQuery("Booking.findByCustomer", Booking.class);
		query.setParameter("customersEmail", customer.getEmail());
		final List<Booking> bookings = query.getResultList();
		if (bookings.isEmpty()) {
			return result;
		}
		result = this.objectFactory.createBookingDOSet(bookings);
		return result;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<SeatDO> getCustomerSeats(CustomerDO customer) {
		List<SeatDO> result = new LinkedList<>();
		if (customer == null) {
			this.logger.warning("doGetCustomerSeats invoked with null parameter.");
			return result;
		}
		final TypedQuery<Seat> query = this.em.createNamedQuery("Seat.findCustomerSeat", Seat.class);
		query.setParameter("customerEmail", customer.getEmail());
		final List<Seat> dbResult = query.getResultList();
		result = this.objectFactory.createSeatDOList(dbResult);
		return result;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<SeatDO> getFreeSeats() {
		final List<Seat> dbResult = doGetFreeSeats();
		final List<SeatDO> result = this.objectFactory.createSeatDOList(dbResult);
		return result;
	}

	@Override
	public LockStrategy getLockStrategy() {
		final LockStrategy lockStrategy = this.lockTracker.getLockStrategy();
		return lockStrategy;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void lockSeat(SeatDO seatDO, CustomerDO customerDO, boolean lock) throws SeatBookedException {
		if (this.lockTracker.getLockStrategy() == LockStrategy.PESSIMISTIC) {
			doLockSeatPessimistic(customerDO, seatDO, lock);
		} else {
			doLockSeatOptimistic(customerDO, seatDO, lock);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public int releaseOldSeatLocks(int maxMinutes) {
		final Query optimisticQuery = this.em.createNamedQuery("OptimisticLock.deleteOldLocks");
		optimisticQuery.setParameter("maxLockMinutes", maxMinutes);
		final int deletedOptimisticLocks = optimisticQuery.executeUpdate();
		this.logger.fine("Number of optimistic locks deleted: " + deletedOptimisticLocks);

		final Query pessimisticQuery = this.em.createNamedQuery("Seat.releaseOldLocks");
		pessimisticQuery.setParameter("maxLockMinutes", maxMinutes);
		final int releasedPessimisticLocks = pessimisticQuery.executeUpdate();
		if (releasedPessimisticLocks > 0) {
			this.logger.info("Number of old pessimistic seat locks released: " + releasedPessimisticLocks);
		} else {
			this.logger.log(Level.FINE, "No seat pessimistic locks found to release");
		}
		return releasedPessimisticLocks;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void releaseSeats(CustomerDO customerDO) {
		if (customerDO == null) {
			this.logger.warning("Tried to remove customer's locks for a null customer.");
			return;
		}
		final Query optimisticQuery = this.em.createNamedQuery("OptimisticLock.deleteCustomersLocks");
		optimisticQuery.setParameter("email", customerDO.getEmail());
		final int deletedOptimisticLocks = optimisticQuery.executeUpdate();
		this.logger.fine("Number of customer's optimistic locks deleted: " + deletedOptimisticLocks);

		final Query pessimisticQuery = this.em.createNamedQuery("Seat.releaseCustomersLocks");
		pessimisticQuery.setParameter("email", customerDO.getEmail());
		final int releasedPessimisticLocks = pessimisticQuery.executeUpdate();
		this.logger.fine("Number of customer's pessimistic locks deleted: " + releasedPessimisticLocks);
	}

	@Override
	public void setLockStrategy(LockStrategy lockStrategy) {
		this.lockTracker.setLockStrategy(lockStrategy);
	}

	private void checkOptimistic(Customer customer, Seat seat) throws SeatBookedException {
		final OptimisticLock optimisticLock = getOptimisticLock(customer, seat);
		if (optimisticLock == null) {
			throw new SeatBookedException("No optimistic lock found when checking.");
		}
		final int originalVersion = optimisticLock.getSeatVersion();
		if (seat.getVersion() != originalVersion) {
			throw new OptimisticLockException("Seat already booked.");
		}
	}

	private void checkPessimistic(Customer customer, Seat seat) {
		if (!(seat.isLocked() && seat.getLockedBy().equals(customer.getEmail()))) {
			throw new PessimisticLockException("Seat not locked or locked by other customer. May already be booked.");
		}
	}

	private String createHash(String text) {
		String hashString = "";
		try {
			MessageDigest md;
			md = MessageDigest.getInstance("MD5");
			final byte[] hash = md.digest(text.getBytes("UTF-8"));
			final StringBuilder sb = new StringBuilder(2 * hash.length);
			for (final byte b : hash) {
				sb.append(String.format("%02x", b & 0xff));
			}
			hashString = sb.toString();
		} catch (final NoSuchAlgorithmException e) {
			this.logger.log(Level.SEVERE, "Hash algoritm missing: " + e.getMessage());
		} catch (final UnsupportedEncodingException e) {
			this.logger.log(Level.SEVERE, "UTF-8 encoding missing: " + e.getMessage());
		}
		return hashString;
	}

	private List<Seat> doGetFreeSeats() {
		final TypedQuery<Seat> query = this.em.createNamedQuery("Seat.findFree", Seat.class);
		query.setLockMode(LockModeType.OPTIMISTIC);
		final List<Seat> dbResult = query.getResultList();
		return dbResult;
	}

	private void doLockSeatOptimistic(CustomerDO customerDO, SeatDO seatDO, boolean lock) throws SeatBookedException {
		final Seat seat = getSeatFromSeatDO(seatDO, getActualLockMode());
		if (seat.isBooked() && lock) {
			throw new SeatBookedException("No use for optimistically locking an already booked seat.");
		}
		final Customer customer = getAndUpdateOrCreateCustomer(customerDO, LockModeType.OPTIMISTIC);
		OptimisticLock optimisticLock = getOptimisticLock(customer, seat);

		if (lock) {
			final int version = seat.getVersion();
			if (optimisticLock == null) {
				optimisticLock = new OptimisticLock(new Date(), version, customer, seat);
				this.em.persist(optimisticLock);
			} else {
				optimisticLock.setLockTime(new Date());
				optimisticLock.setSeatVersion(version);
			}
		} else {
			if (optimisticLock != null) {
				this.em.remove(optimisticLock);
			}
		}
	}

	private void doLockSeatPessimistic(CustomerDO customerDO, SeatDO seatDO, boolean lock) throws SeatBookedException {
		final Seat seatToLock = getSeatFromSeatDO(seatDO, getActualLockMode());
		final String email = customerDO.getEmail();
		if (seatToLock == null) {
			this.logger.warning("Tried to lock non-existing seat");
			return;
		}
		if (seatToLock.isLocked() && !email.equals(seatToLock.getLockedBy())) {
			throw new SeatBookedException("Seat is already locked");
		}
		if (lock && seatToLock.isBooked()) {
			throw new SeatBookedException("Seat is already booked");
		}

		seatToLock.setLocked(lock);
		if (lock) {
			seatToLock.setLockTime(new Date());
			seatToLock.setLockedBy(email);
		} else {
			seatToLock.setLockTime(null);
			seatToLock.setLockedBy(null);
		}
	}

	private LockModeType getActualLockMode() {
		final LockModeType lockMode = this.lockTracker.getLockStrategy() == LockStrategy.OPTIMISTIC
				? LockModeType.OPTIMISTIC : LockModeType.PESSIMISTIC_WRITE;
		return lockMode;
	}

	private Customer getAndUpdateOrCreateCustomer(CustomerDO customerDO, LockModeType lockMode) {
		Customer customer = getCustomerFromCustomerDO(customerDO, lockMode);
		if (customer != null) {
			customer.setFirstName(customerDO.getFirstName());
			customer.setLastName(customerDO.getLastName());
			customer.setTelephoneNumber(customerDO.getTelephoneNumber());
		} else {
			customer = this.objectFactory.createCustomer(customerDO);
			this.em.persist(customer);
		}
		return customer;
	}

	private Customer getCustomerFromCustomerDO(CustomerDO customerDO, LockModeType lockMode) {
		if (customerDO == null) {
			this.logger.warning("Tried to get customer from db from a null domain object.");
			return null;
		}
		return getCustomerFromEmail(customerDO.getEmail(), lockMode);
	}

	private Customer getCustomerFromEmail(String email, LockModeType lockMode) {
		if (email == null || email.isEmpty()) {
			this.logger.warning("Tried to get customer from db from  a null email address.");
			return null;
		}
		final TypedQuery<Customer> query = this.em.createNamedQuery("Customer.findByEmail", Customer.class);
		query.setParameter("email", email);
		query.setLockMode(lockMode);
		final List<Customer> resultList = query.getResultList();
		if (resultList.isEmpty()) {
			return null;
		}
		return resultList.get(0);
	}

	private OptimisticLock getOptimisticLock(Customer customer, Seat seat) {
		final TypedQuery<OptimisticLock> query = this.em.createNamedQuery("OptimisticLock.findLock",
				OptimisticLock.class);
		query.setParameter("email", customer.getEmail());
		query.setParameter("seatNumber", seat.getSeatNumber());
		final List<OptimisticLock> optimisticLocks = query.getResultList();
		if (optimisticLocks.isEmpty()) {
			return null;
		}
		final OptimisticLock optimisticLock = optimisticLocks.get(0);
		return optimisticLock;
	}

	private Seat getSeatFromSeatDO(SeatDO seatDO, LockModeType lockMode) {
		final TypedQuery<Seat> query = this.em.createNamedQuery("Seat.findSeatByNumber", Seat.class);
		query.setLockMode(lockMode);
		query.setParameter("seatNumber", seatDO.getSeatNumber());
		final List<Seat> queryResult = query.getResultList();
		if (queryResult.isEmpty()) {
			return null;
		} else {
			final Seat seat = queryResult.get(0);
			return seat;
		}
	}

	private Map<Integer, Seat> getSeatsFromSeatDOs(Collection<SeatDO> seatDOs, LockModeType lockMode) {
		final Map<Integer, Seat> result = new HashMap<>();
		if (seatDOs == null || seatDOs.isEmpty()) {
			return result;
		}

		final TypedQuery<Seat> query = this.em.createNamedQuery("Seat.findSeatByNumber", Seat.class);
		query.setLockMode(lockMode);
		for (final SeatDO seatDO : seatDOs) {
			query.setParameter("seatNumber", seatDO.getSeatNumber());
			final List<Seat> queryResult = query.getResultList();
			if (!queryResult.isEmpty()) {
				final Seat seat = queryResult.get(0);
				result.put(new Integer(seat.getSeatNumber()), seat);
			}
		}
		return result;
	}

}

package com.cgi.osd.transactiondemo.persistence;

import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.cgi.osd.transactiondemo.logic.domainobject.LockStrategy;

/**
 * This class is responsible for keeping locking information as versions for
 * optimistic strategy and the selected strategy itself.
 *
 * @author anderssonhr
 *
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Lock(LockType.READ)
public class LockTrackerBean implements LockTracker {

    private LockStrategy lockStrategy;

    /*
     * (non-Javadoc)
     *
     * @see
     * com.cgi.osd.transactiondemo.persistence.LockTracker#getLockStrategy()
     */
    @Override
    public LockStrategy getLockStrategy() {
	return this.lockStrategy;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.cgi.osd.transactiondemo.persistence.LockTracker#setLockStrategy(com.
     * cgi.osd.transactiondemo.persistence.LockTrackerBean.LockStrategy )
     */
    @Override
    @Lock(LockType.WRITE)
    public void setLockStrategy(LockStrategy lockStrategy) {
	this.lockStrategy = lockStrategy;
    }

    @PostConstruct
    private void init() {
	final String systemDefaultLock = System.getProperty("default-lock");
	if (systemDefaultLock != null && "pessimistic".equalsIgnoreCase(systemDefaultLock)) {
	    this.lockStrategy = LockStrategy.PESSIMISTIC;
	} else {
	    this.lockStrategy = LockStrategy.OPTIMISTIC;
	}
    }

}

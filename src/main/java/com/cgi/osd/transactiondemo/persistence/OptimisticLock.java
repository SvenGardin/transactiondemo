package com.cgi.osd.transactiondemo.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the optimistic_lock database table.
 *
 */
@Entity
@Table(name = "optimistic_lock")
@NamedQueries({
	@NamedQuery(name = "OptimisticLock.findAll", query = "SELECT o FROM OptimisticLock o"),
	@NamedQuery(name = "OptimisticLock.findLock", query = "SELECT o FROM OptimisticLock o JOIN FETCH o.customer c JOIN FETCH o.seat s WHERE c.email = :email AND s.seatNumber = :seatNumber") })
@NamedNativeQueries({
	@NamedNativeQuery(name = "OptimisticLock.deleteOldLocks", query = "DELETE from `optimistic_lock` WHERE `lock_time` + SEC_TO_TIME(60 * :maxLockMinutes) <= NOW()"),
	@NamedNativeQuery(name = "OptimisticLock.deleteCustomersLocks", query = "DELETE FROM `optimistic_lock` "
		+ "WHERE EXISTS( SELECT * FROM `customer` `c` WHERE `c`.`p_key` = `optimistic_lock`.`p_key_customer` AND `c`.`email` = :email)") })
public class OptimisticLock extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lock_time")
    private Date lockTime;

    @Column(name = "seat_version", nullable = false)
    private int seatVersion;

    // bi-directional many-to-one association to Customer
    @ManyToOne
    @JoinColumn(name = "p_key_customer", nullable = false)
    private Customer customer;

    // bi-directional many-to-one association to Seat
    @ManyToOne
    @JoinColumn(name = "p_key_seat", nullable = false)
    private Seat seat;

    public OptimisticLock() {
    }

    /**
     * This is the normally used constructor.
     *
     * @param lockTime
     *            the time stamp now.
     * @param seatVersion
     *            the optimistic lock version of the seat.
     * @param customer
     *            the customer who has selected the seat.
     * @param seat
     */
    public OptimisticLock(Date lockTime, int seatVersion, Customer customer, Seat seat) {
	super();
	this.lockTime = lockTime;
	this.seatVersion = seatVersion;
	this.customer = customer;
	this.seat = seat;
    }

    public Customer getCustomer() {
	return this.customer;
    }

    public Date getLockTime() {
	return this.lockTime;
    }

    public Seat getSeat() {
	return this.seat;
    }

    public int getSeatVersion() {
	return this.seatVersion;
    }

    public void setCustomer(Customer customer) {
	this.customer = customer;
    }

    public void setLockTime(Date lockTime) {
	this.lockTime = lockTime;
    }

    public void setSeat(Seat seat) {
	this.seat = seat;
    }

    public void setSeatVersion(int seatVersion) {
	this.seatVersion = seatVersion;
    }

}
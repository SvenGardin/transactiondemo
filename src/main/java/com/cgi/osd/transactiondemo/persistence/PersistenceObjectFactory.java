package com.cgi.osd.transactiondemo.persistence;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;

import com.cgi.osd.transactiondemo.logic.domainobject.BookingDO;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;

/**
 * This class is responsible for creating objects by converting between domain object classes and entity classes.
 *
 * @author anderssonhr
 *
 */
@ApplicationScoped
public class PersistenceObjectFactory {

	public BookingDO createBookingDO(Booking booking) {
		if (booking == null) {
			return null;
		}
		final String bookingReference = booking.getBookingReference();
		final String customerEmail = booking.getCustomer().getEmail();
		final Set<Seat> seats = booking.getSeats();
		final Set<Integer> seatNumbers = new HashSet<>();
		for (final Seat seat : seats) {
			final Integer seatNumber = new Integer(seat.getSeatNumber());
			seatNumbers.add(seatNumber);
		}
		final BookingDO bookingDO = new BookingDO(customerEmail, seatNumbers, bookingReference);
		return bookingDO;
	}

	public Set<BookingDO> createBookingDOSet(Collection<Booking> bookings) {
		final Set<BookingDO> bookingDOs = new HashSet<>();
		for (final Booking booking : bookings) {
			final BookingDO bookingDO = createBookingDO(booking);
			bookingDOs.add(bookingDO);
		}
		return bookingDOs;
	}

	public Customer createCustomer(CustomerDO customerDO) {
		final Customer customer = new Customer(customerDO.getEmail(), customerDO.getFirstName(),
				customerDO.getLastName(), customerDO.getTelephoneNumber());
		customer.setBookings(new LinkedList<Booking>());
		// A new customer doesn't have bookings.
		return customer;
	}

	public CustomerDO createCustomerDO(Customer customer) {
		if (customer == null) {
			return null;
		}
		final CustomerDO customerDO = new CustomerDO(customer.getFirstName(), customer.getLastName(),
				customer.getEmail(), customer.getTelephoneNumber());
		final List<Booking> bookings = customer.getBookings();
		if (bookings != null && !bookings.isEmpty()) {
			final Set<String> bookingReferences = new HashSet<>();
			for (final Booking booking : bookings) {
				final String bookingReference = booking.getBookingReference();
				bookingReferences.add(bookingReference);
			}
			customerDO.setBookingReferences(bookingReferences);
		}
		return customerDO;
	}

	public SeatDO createSeatDO(Seat seat) {
		if (seat == null) {
			return null;
		}
		final SeatDO seatDO = new SeatDO(seat.getSeatNumber());

		final boolean pessimisticLocked = seat.isLocked();
		seatDO.setPessimisticLocked(pessimisticLocked);

		final Booking booking = seat.getBooking();
		if (booking != null) {
			seatDO.setBookingReference(booking.getBookingReference());
		}
		return seatDO;
	}

	public List<SeatDO> createSeatDOList(Collection<Seat> seats) {
		final List<SeatDO> result = new LinkedList<>();
		for (final Seat seat : seats) {
			if (seat != null) {
				result.add(createSeatDO(seat));
			}
		}
		return result;
	}

}

package com.cgi.osd.transactiondemo.persistence;

import java.util.logging.Logger;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.persistence.OptimisticLockException;
import javax.persistence.PessimisticLockException;

import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;
import com.cgi.osd.transactiondemo.logic.exception.SeatBookedException;

/**
 * This class is intended to be used as an interceptor around persistence calls in order to convert exceptions from
 * persistence specific exceptions to business logic exceptions.
 *
 */
public class ExceptionInterceptor {

    @Inject
    private Logger logger;

    @AroundInvoke
    public Object convertException(InvocationContext ctx) throws Exception {
	try {
	    return ctx.proceed();
	} catch (final Exception e) {
	    final String methodName = ctx.getMethod().getName();
	    final Exception exception = handleException(e, methodName);
	    throw exception;
	}
    }

    private Exception handleException(Throwable e, String methodName) {
	final Throwable throwableOptimistic = unrollExceptionToClass(e, OptimisticLockException.class);
	if (throwableOptimistic != null) {
	    logger.warning("OptimisticLockException in " + methodName + ": " + throwableOptimistic.getMessage());
	    return new SeatBookedException(throwableOptimistic.getMessage());
	}

	final Throwable throwablePessimistic = unrollExceptionToClass(e, PessimisticLockException.class);
	if (throwablePessimistic != null) {
	    logger.warning("PessimisticLockException in " + methodName + ": " + throwablePessimistic.getMessage());
	    return new SeatBookedException(throwablePessimistic.getMessage());
	}

	final Throwable throwableUnrolled = unrollExceptionToLast(e);
	logger.warning("Got exception in " + methodName + ": " + throwableUnrolled.getMessage());
	return new PersistenceOperationException(throwableUnrolled.getMessage());
    }

    private Throwable unrollExceptionToClass(Throwable exception, Class<? extends Throwable> expected) {
	while (exception != null && exception != exception.getCause()) {
	    if (expected.isInstance(exception)) {
		return exception;
	    }
	    exception = exception.getCause();
	}
	return null;
    }

    private Throwable unrollExceptionToLast(Throwable exception) {
	while (exception != null && exception != exception.getCause()) {
	    if (exception.getCause() == null || exception == exception.getCause()) {
		return exception;
	    }
	    exception = exception.getCause();
	}
	return exception;
    }

}

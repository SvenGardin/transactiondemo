package com.cgi.osd.transactiondemo.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the customer database table.
 *
 */
@Entity
@Table(name = "customer")
@NamedQueries({ @NamedQuery(name = "Customer.findAll", query = "SELECT c FROM Customer c"),
    @NamedQuery(name = "Customer.findByEmail", query = "SELECT c FROM Customer c WHERE c.email = :email") })
public class Customer extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(length = 255, nullable = false, unique = true)
    private String email;

    @Column(name = "first_name", nullable = false, length = 255)
    private String firstName;

    @Column(name = "last_name", nullable = false, length = 255)
    private String lastName;

    @Column(name = "telephone_number", length = 255)
    private String telephoneNumber;

    // bi-directional many-to-one association to Booking
    @OneToMany(mappedBy = "customer")
    private List<Booking> bookings;

    // bi-directional many-to-one association to OptimisticLock
    @OneToMany(mappedBy = "customer", fetch = FetchType.EAGER)
    private List<OptimisticLock> optimisticLocks;

    public Customer() {
    }

    /**
     * Creates an instance.
     *
     * @param email
     *            customer's email address.
     * @param firstName
     *            customer's first name.
     * @param lastName
     *            customer's last name.
     * @param telephoneNumber
     *            customer's telephone number.
     */
    public Customer(String email, String firstName, String lastName, String telephoneNumber) {
	super();
	this.email = email;
	this.firstName = firstName;
	this.lastName = lastName;
	this.telephoneNumber = telephoneNumber;
    }

    public Booking addBooking(Booking booking) {
	getBookings().add(booking);
	booking.setCustomer(this);

	return booking;
    }

    /**
     * @param email
     * @param firstName
     * @param lastName
     * @param telephoneNumber
     * @param bookings
     */

    /**
     * Copies all updatable data from another instance.
     *
     * @param other
     *            the source instance.
     */
    public void copyCustomer(Customer other) {
	this.firstName = other.getFirstName();
	this.lastName = other.getLastName();
	this.telephoneNumber = other.getTelephoneNumber();
    }

    public List<Booking> getBookings() {
	return this.bookings;
    }

    public String getEmail() {
	return this.email;
    }

    public String getFirstName() {
	return this.firstName;
    }

    public String getLastName() {
	return this.lastName;
    }

    public List<OptimisticLock> getOptimisticLocks() {
	return this.optimisticLocks;
    }

    public String getTelephoneNumber() {
	return this.telephoneNumber;
    }

    public Booking removeBooking(Booking booking) {
	getBookings().remove(booking);
	booking.setCustomer(null);

	return booking;
    }

    public void setBookings(List<Booking> bookings) {
	this.bookings = bookings;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public void setOptimisticLocks(List<OptimisticLock> optimisticLocks) {
	this.optimisticLocks = optimisticLocks;
    }

    public void setTelephoneNumber(String telephoneNumber) {
	this.telephoneNumber = telephoneNumber;
    }

}

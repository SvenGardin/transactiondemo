package com.cgi.osd.transactiondemo.persistence;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the seat database table.
 *
 */
@Entity
@Table(name = "seat")
@NamedQueries({
	@NamedQuery(name = "Seat.findAll", query = "SELECT s FROM Seat s"),
	@NamedQuery(name = "Seat.findFree", query = "SELECT s FROM Seat s WHERE s.booking IS NULL AND s.locked = false ORDER BY s.seatNumber"),
	@NamedQuery(name = "Seat.findCustomerSeat", query = "SELECT s FROM Seat s LEFT JOIN s.booking b LEFT JOIN b.customer c "
		+ "WHERE ((s.booking IS NULL OR c.email = :customerEmail) AND s.locked = false) "
		+ "OR (s.locked = true AND s.lockedBy = :customerEmail) ORDER BY s.seatNumber"),
	@NamedQuery(name = "Seat.findBooked", query = "SELECT s FROM Seat s WHERE s.booking IS NOT NULL ORDER BY s.seatNumber"),
	@NamedQuery(name = "Seat.findFreeSeat", query = "SELECT s FROM Seat s WHERE s.booking IS NULL and s.seatNumber = :seatNumber"),
	@NamedQuery(name = "Seat.findSeatByNumber", query = "SELECT s FROM Seat s WHERE s.seatNumber = :seatNumber") })
@NamedNativeQueries({
	@NamedNativeQuery(name = "Seat.releaseOldLocks", query = "UPDATE `seat` "
	    + "SET `update_date` = NOW(), `locked` = false, `lock_time`= NULL, `locked_by` = NULL, `version` = `version` + 1 "
	    + "WHERE `lock_time` + SEC_TO_TIME(60 * :maxLockMinutes) <= NOW()"),
	@NamedNativeQuery(name = "Seat.releaseCustomersLocks", query = "UPDATE `seat` "
		    + "SET `update_date` = NOW(), `locked` = false, `lock_time`= NULL, `locked_by` = NULL, `version` = `version` + 1 "
		    + "WHERE `locked_by` = :email") })
public class Seat extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "seat_number", nullable = false, unique = true)
    private int seatNumber;

    // bi-directional many-to-one association to Booking
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "p_key_booking", referencedColumnName = "p_key")
    private Booking booking;

    @Column(name = "locked", nullable = false)
    private boolean locked;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lock_time", nullable = true)
    private Date lockTime;

    @Column(name = "locked_by", nullable = true)
    private String lockedBy;

    // bi-directional many-to-one association to OptimisticLock
    @OneToMany(mappedBy = "seat", fetch = FetchType.EAGER)
    private List<OptimisticLock> optimisticLocks;

    public Seat() {
    }

    /**
     * Constructs an instance with all mandatory data properties.
     *
     * @param booked
     *            booked status, booked of not.
     * @param createDate
     *            time of creation.
     * @param seatNumber
     *            the seat number.
     * @param updateDate
     *            same as creation time.
     */
    public Seat(int seatNumber) {
	super();
	this.seatNumber = seatNumber;
    }

    public Booking getBooking() {
	return this.booking;
    }

    public String getLockedBy() {
	return this.lockedBy;
    }

    public Date getLockTime() {
	return this.lockTime;
    }

    public List<OptimisticLock> getOptimisticLocks() {
	return this.optimisticLocks;
    }

    public int getSeatNumber() {
	return this.seatNumber;
    }

    public boolean isBooked() {
	return this.booking != null;
    }

    public boolean isLocked() {
	return this.locked;
    }

    public void setBooking(Booking booking) {
	this.booking = booking;
    }

    public void setLocked(boolean locked) {
	this.locked = locked;
    }

    public void setLockedBy(String lockedBy) {
	this.lockedBy = lockedBy;
    }

    public void setLockTime(Date lockTime) {
	this.lockTime = lockTime;
    }

    public void setOptimisticLocks(List<OptimisticLock> optimisticLocks) {
	this.optimisticLocks = optimisticLocks;
    }

    public void setSeatNumber(int seatNumber) {
	this.seatNumber = seatNumber;
    }

}
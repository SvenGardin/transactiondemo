package com.cgi.osd.transactiondemo.persistence;

import com.cgi.osd.transactiondemo.logic.domainobject.LockStrategy;

/**
 * This interface define methods for keeping track of the actual lock strategy..
 *
 * @author anderssonhr
 *
 */
public interface LockTracker {

    /**
     * This method returns the actual lock strategy.
     *
     * @return the actual lock strategy.
     */
    public LockStrategy getLockStrategy();

    /**
     * This method sets the lock strategy.
     *
     * @param lockStrategy
     *            the actual strategy.
     */
    public void setLockStrategy(LockStrategy lockStrategy);

}
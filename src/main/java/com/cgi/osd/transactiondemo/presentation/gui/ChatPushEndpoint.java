package com.cgi.osd.transactiondemo.presentation.gui;

import java.util.logging.Logger;

import org.primefaces.push.EventBus;
import org.primefaces.push.RemoteEndpoint;
import org.primefaces.push.annotation.OnClose;
import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.annotation.OnOpen;
import org.primefaces.push.annotation.PushEndpoint;
import org.primefaces.push.impl.JSONEncoder;

/**
 * This class is responsible for being an end point for PrimeFaces chat push.
 *
 * @author anderssonhr
 *
 */
@PushEndpoint("/chat")
public class ChatPushEndpoint {

    private final Logger logger = Logger.getLogger(ChatPushEndpoint.class.getName());

    /**
     * For now only implemented for logging purposes.
     *
     * @param r
     * @param e
     */
    @OnClose
    public void onClose(RemoteEndpoint r, EventBus e) {
	this.logger.fine("onClose called");
    }

    /**
     * This method is an push end point. It must exist and return something even if the returned value isn't used by the client.
     *
     * @param message
     *            the chat message. Must not be null.
     *
     * @return the chat message.
     */
    @OnMessage(encoders = { JSONEncoder.class })
    public String onMessage(String message) {
	this.logger.fine("onMessage called with message: " + message);
	return message;
    }

    /**
     * For now only implemented for logging purposes.
     *
     * @param r
     * @param e
     */
    @OnOpen
    public void onOpen(RemoteEndpoint r, EventBus e) {
	this.logger.fine("onOpen called");
    }
}

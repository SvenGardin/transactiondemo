package com.cgi.osd.transactiondemo.presentation.gui;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.PreDestroy;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewExpiredException;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.component.datatable.DataTable;

import com.cgi.osd.transactiondemo.logic.BookingService;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.SeatBookedException;

/**
 * This class is responsible for the controller part of the MVC pattern for the booking process.
 *
 */
@SessionScoped
@Named
public class BookingController implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger logger;

	@Inject
	private FacesContext facesContext;

	@Inject
	private BookingData bookingData;

	@Inject
	private BookingService bookingService;

	@Inject
	private PushController pushController;

	public void confirmBookings(ActionEvent ev) {
		try {
			final CustomerDO customer = this.bookingData.getCustomer();
			this.bookingData.setCustomer(customer);
			final Set<SeatDO> seatsToBook = this.bookingData.getSeatsToBook();
			this.bookingService.createBookings(customer, seatsToBook);
			this.bookingData.updateSeats();
			this.bookingData.clearSeatsToBook();
		} catch (final SeatBookedException e) {
			this.facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bokning misslyckades!",
					"Plats bokad av annan kund."));
		}
	}

	public String getEmail() {
		if (this.bookingData.getCustomer() == null) {
			return "";
		}
		return this.bookingData.getCustomer().getEmail();
	}

	public String getFirstName() {
		if (this.bookingData.getCustomer() == null) {
			return "";
		}
		return this.bookingData.getCustomer().getFirstName();
	}

	public String getLastName() {
		if (this.bookingData.getCustomer() == null) {
			return "";
		}
		return this.bookingData.getCustomer().getLastName();
	}

	public List<SeatSelection> getSeatSelects() {
		Map<Integer, SeatSelection> response = this.bookingData.getSeatSelections();
		if (response.isEmpty()) {
			updateSeatsFromDb();
			response = this.bookingData.getSeatSelections();
		}
		final Collection<SeatSelection> values = response.values();
		final List<SeatSelection> result = new LinkedList<>(values);
		Collections.sort(result);
		return result;
	}

	public String getTelephoneNumber() {
		if (this.bookingData.getCustomer() == null) {
			return "";
		}
		return this.bookingData.getCustomer().getTelephoneNumber();
	}

	public boolean isCustomerEntered() {
		final CustomerDO customer = this.bookingData.getCustomer();
		if (customer == null) {
			return false;
		}
		return customer.isMandatorySet();
	}

	public void onEmailChanged(ValueChangeEvent ev) {
		final String email = (String) ev.getNewValue();
		this.bookingData.fetchOrCreateCustomer(email);
		updateSeatsFromDb();
	}

	public void onSelectionChanged(ValueChangeEvent ev) {
		try {
			this.logger.fine("Lock button pressed.");
			final UIComponent component = ev.getComponent();

			final DataTable table = (DataTable) component.getParent().getParent();
			final Boolean newBookingStatus = (Boolean) ev.getNewValue();
			final SeatSelection seatSelect = (SeatSelection) table.getRowData();
			final SeatDO selectedSeat = seatSelect.getSeat();
			final Integer seatNumber = selectedSeat.getSeatNumber();
			final boolean selected = newBookingStatus.booleanValue();

			this.bookingData.setSelected(seatNumber, selected);
			this.bookingService.lockSeat(selectedSeat, this.bookingData.getCustomer(), selected);

			this.logger.fine("Lock changed to " + newBookingStatus.toString() + " on seat number "
					+ selectedSeat.getSeatNumber());
		} catch (final SeatBookedException e) {
			this.facesContext.addMessage("bookingMessage", new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Bokning misslyckades!", "Plats bokad av annan kund."));
			this.pushController.updateSeatTables();
		}
	}

	public void setEmail(String email) {
		// Email is taken care of in onEmailChanged.
	}

	public void setFirstName(String firstName) {
		this.bookingData.getCustomer().setFirstName(firstName);
	}

	public void setLastName(String lastName) {
		this.bookingData.getCustomer().setLastName(lastName);
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.bookingData.getCustomer().setTelephoneNumber(telephoneNumber);
	}

	/**
	 * This method is for test purposes only.
	 */
	public void throwViewExpiredException() {
		throw new ViewExpiredException("A ViewExpiredException!",
				FacesContext.getCurrentInstance().getViewRoot().getViewId());
	}

	/**
	 * This method updates the seat data for the web GUI.
	 */
	public void updateSeatsFromDb() {
		this.logger.fine("updateSeatsFromDb called.");
		this.bookingData.updateSeats();
		this.logger.fine("updateSeatsFromDb ready.");
	}

	@PreDestroy
	private void onSessionTimeout() {
		final CustomerDO customer = this.bookingData.getCustomer();
		if (customer != null) {
			try {
				final Collection<SeatDO> seatsToRelease = this.bookingData.getSeatsToBook();
				this.bookingService.releaseSeats(customer, seatsToRelease);
				this.logger.fine("Session time out");
			} catch (final SeatBookedException e) {
				this.logger.warning("Failed to release customer's locks at session timeout.");
			}
		}
	}

}

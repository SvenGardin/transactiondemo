package com.cgi.osd.transactiondemo.presentation.gui;

import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

import com.cgi.osd.transactiondemo.logic.BookingService;

/**
 * This class is responsible for creating push events when notified about booking changes.
 *
 * @author anderssonhr
 *
 */
@Singleton
@Startup
@DependsOn("BookingServiceBean")
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.NEVER)
@Lock(LockType.READ)
public class PushControllerBean implements Observer, PushController {

    private boolean pushDisabled;
    private EventBus eventBus;

    @Inject
    private Logger logger;

    @Inject
    private BookingService bookingService;

    @Override
    public boolean isPushDisabled() {
	return pushDisabled;
    }

    @Override
    @Lock(LockType.WRITE)
    public void setPushDisabled(boolean pushDisabled) {
	this.pushDisabled = pushDisabled;
    }

    @Override
    public void update(Observable o, Object arg) {
	if (pushDisabled) {
	    return;
	}
	final Integer numberOfSeats = (Integer) arg;
	performPush(numberOfSeats);
    }

    @Override
    public void updateSeatTables() {
	performPush(new Integer(0));
    }

    @PostConstruct
    private void init() {
	bookingService.registerObserver(this);
	eventBus = EventBusFactory.getDefault().eventBus();
    }

    @PreDestroy
    private void onDestroy() {
	bookingService.unregisterObserver(this);
    }

    private void performPush(Integer numberOfSeats) {
	eventBus.publish("/seatList", numberOfSeats);
	logger.fine("Push performed.");
    }

}

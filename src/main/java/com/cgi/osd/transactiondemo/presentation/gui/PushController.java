package com.cgi.osd.transactiondemo.presentation.gui;

/**
 * This interface defines the public methods of the Push controller.
 *
 * @author anderssonhr
 *
 */
public interface PushController {

    /**
     * This method is used for getting information about push status.
     *
     * @return true is push is disabled.
     */
    public boolean isPushDisabled();

    /**
     * This method sets the push status.
     *
     * @param pushDisabled
     *            true if push should be disabled.
     */
    public void setPushDisabled(boolean pushDisabled);

    /**
     * This method pushes an event to the clients that causes them to update their seat tables. The push is performed regardless of push
     * disable status.
     */
    public void updateSeatTables();
}
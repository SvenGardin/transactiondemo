package com.cgi.osd.transactiondemo.presentation.gui;

import java.util.logging.Logger;

import org.primefaces.push.EventBus;
import org.primefaces.push.RemoteEndpoint;
import org.primefaces.push.annotation.OnClose;
import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.annotation.OnOpen;
import org.primefaces.push.annotation.PushEndpoint;
import org.primefaces.push.impl.JSONEncoder;

/**
 * This class is responsible for being an end point for PrimeFaces push.
 *
 * @author anderssonhr
 *
 */
@PushEndpoint("/seatList")
public class SeatListPushEndpoint {

    private final Logger logger = Logger.getLogger(SeatListPushEndpoint.class.getName());

    /**
     * For now only implemented for logging purposes.
     *
     * @param r
     * @param e
     */
    @OnClose
    public void onClose(RemoteEndpoint r, EventBus e) {
	this.logger.fine("onClose called");
    }

    /**
     * This method is an push end point. It must exist and return something even if the returned value isn't used by the client.
     *
     * @param numberOfSeats
     *            the number of updated seats. Must not be null.
     *
     * @return the number of updated seats.
     */
    @OnMessage(encoders = { JSONEncoder.class })
    public String onMessage(Integer numberOfSeats) {
	this.logger.fine("onMessage called.");
	final String response = numberOfSeats.toString();
	return response;
    }

    /**
     * For now only implemented for logging purposes.
     *
     * @param r
     * @param e
     */
    @OnOpen
    public void onOpen(RemoteEndpoint r, EventBus e) {
	this.logger.fine("onOpen called");
    }
}

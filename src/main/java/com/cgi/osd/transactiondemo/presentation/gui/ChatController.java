/**
 *
 */
package com.cgi.osd.transactiondemo.presentation.gui;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

/**
 * This class is responsible for the controller part of the MVC pattern applied to the chat process.
 *
 * @author anderssonhr
 *
 */
@SessionScoped
@Named
public class ChatController implements Serializable {

    private static final long serialVersionUID = 1L;
    private EventBus eventBus;
    private String textInput;

    public String getTextInput() {
	return this.textInput;
    }

    public void sendMessageToClient() {
	if (this.textInput != null || !this.textInput.isEmpty()) {
	    this.eventBus.publish("/chat", this.textInput);
	}
    }

    public void setTextInput(String textInput) {
	this.textInput = textInput;
    }

    @PostConstruct
    private void init() {
	this.eventBus = EventBusFactory.getDefault().eventBus();
    }

}

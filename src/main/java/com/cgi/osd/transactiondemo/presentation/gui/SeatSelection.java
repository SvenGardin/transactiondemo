package com.cgi.osd.transactiondemo.presentation.gui;

import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;

/**
 * This class is responsible for holding seat domain data plus information about if the customer has intended to book this seat.
 *
 * @author anderssonhr
 *
 */
public class SeatSelection implements Comparable<SeatSelection> {

    private SeatDO seat;
    private boolean selected;

    public SeatSelection(SeatDO seat, boolean selected) {
	super();
	this.seat = seat;
	this.selected = selected;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(SeatSelection o) {
	return o.getSeat().compareTo(getSeat());
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	final SeatSelection other = (SeatSelection) obj;
	if (this.seat == null) {
	    if (other.seat != null) {
		return false;
	    }
	} else if (!this.seat.equals(other.seat)) {
	    return false;
	}
	return true;
    }

    public SeatDO getSeat() {
	return this.seat;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + (this.seat == null ? 0 : this.seat.hashCode());
	return result;
    }

    public boolean isSelected() {
	return this.selected || this.seat.isBooked();
    }

    public void setSeat(SeatDO seat) {
	this.seat = seat;
    }

    public void setSelected(boolean selected) {
	this.selected = selected;
    }

}

/**
 *
 */
package com.cgi.osd.transactiondemo.presentation.gui;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.cgi.osd.transactiondemo.logic.BookingService;
import com.cgi.osd.transactiondemo.logic.domainobject.LockStrategy;

/**
 * This class is responsible for the controller part of the MVC pattern applied to the setting process. Note that settings are application
 * global.
 *
 * @author anderssonhr
 *
 */
@RequestScoped
@Named
public class SettingController {

    @Inject
    private PushController pushController;

    @Inject
    private BookingService bookingService;

    public boolean getPushDisabled() {
	final boolean value = this.pushController.isPushDisabled();
	return value;
    }

    public String getStrategy() {
	final LockStrategy lockStrategy = this.bookingService.getLockStrategy();
	String lockStrategyString;
	switch (lockStrategy) {
	case OPTIMISTIC:
	    lockStrategyString = "optimistic";
	    break;
	case PESSIMISTIC:
	    lockStrategyString = "pessimistic";
	    break;
	default:
	    lockStrategyString = "optimistic";
	    break;
	}
	return lockStrategyString;
    }

    public void setPushDisabled(boolean value) {
	this.pushController.setPushDisabled(value);
    }

    public void setStrategy(String strategyString) {
	LockStrategy lockStrategy;
	switch (strategyString) {
	case "optimistic":
	    lockStrategy = LockStrategy.OPTIMISTIC;
	    break;
	case "pessimistic":
	    lockStrategy = LockStrategy.PESSIMISTIC;
	    break;
	default:
	    lockStrategy = LockStrategy.OPTIMISTIC;
	    break;
	}
	this.bookingService.setLockStrategy(lockStrategy);
    }

}

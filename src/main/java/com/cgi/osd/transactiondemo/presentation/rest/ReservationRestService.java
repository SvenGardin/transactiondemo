package com.cgi.osd.transactiondemo.presentation.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cgi.osd.transactiondemo.logic.BookingService;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;

@RequestScoped
@Path("/reservation")
public class ReservationRestService {

	@Inject
	private BookingService bookingService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/free-seats")
	public List<SeatDO> getFreeSeats() {
		final List<SeatDO> freeSeats = this.bookingService.getFreeSeats();
		return freeSeats;
	}

}

package com.cgi.osd.transactiondemo.presentation.gui;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.cgi.osd.transactiondemo.logic.BookingService;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;

/**
 * This class is responsible for holding booking data that needs to be cached for some reason. Should only be accessed
 * using the BookingController to make sure that only one instance will be connected to session.
 *
 */
@Dependent
public class BookingData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private BookingService bookingService;
    @Inject
    private Logger logger;
    private Map<Integer, SeatSelection> seatSelects;
    private CustomerDO customer;

    public void clearSeatsToBook() {
	for (final SeatSelection seatAndLocalSelect : this.seatSelects.values()) {
	    seatAndLocalSelect.setSelected(false);
	}
    }

    public void fetchOrCreateCustomer(String email) {
	final CustomerDO foundCustomer = this.bookingService.getCustomerFromEmail(email);
	if (foundCustomer != null) {
	    this.customer = foundCustomer;
	} else {
	    this.customer = new CustomerDO(email);
	}
    }

    public CustomerDO getCustomer() {
	return this.customer;
    }

    public Map<Integer, SeatSelection> getSeatSelections() {
	return this.seatSelects;
    }

    public Set<SeatDO> getSeatsToBook() {
	final Set<SeatDO> result = new HashSet<>();
	for (final SeatSelection seatSelect : this.seatSelects.values()) {
	    if (seatSelect.isSelected()) {
		result.add(seatSelect.getSeat());
	    }
	}
	return result;
    }

    public void setCustomer(CustomerDO customer) {
	this.customer = customer;
    }

    public void setSeatSelections(Map<Integer, SeatSelection> seatSelects) {
	this.seatSelects = seatSelects;
    }

    public void setSelected(Integer seatNumber, boolean selected) {
	if (seatNumber == null) {
	    this.logger.warning("Tried to change selection  on null seat number");
	    return;
	}

	final SeatSelection seatAndSelect = this.seatSelects.get(seatNumber);
	if (seatAndSelect == null) {
	    this.logger.warning("Tried to change lock on seat not found in map");
	}
	seatAndSelect.setSelected(selected);
    }

    public void updateSeats() {
	final Map<Integer, SeatSelection> oldMap = this.seatSelects;
	this.seatSelects = new HashMap<>();

	final List<SeatDO> seats = this.bookingService.getCustomersAndFreeSeat(this.customer);
	for (final SeatDO seat : seats) {
	    final SeatSelection newSeatSelect = new SeatSelection(seat, false);
	    final SeatSelection existingSeatSelect = oldMap.get(seat.getSeatNumber());
	    boolean selected = false;
	    if (existingSeatSelect != null) {
		selected = existingSeatSelect.isSelected();
		newSeatSelect.setSelected(selected);
	    }
	    this.seatSelects.put(new Integer(seat.getSeatNumber()), newSeatSelect);
	}
    }

    @PostConstruct
    private void init() {
	this.customer = null;
	this.seatSelects = new HashMap<>();
    }
}

function handleMessage(message) {
	PF('growlPush').renderMessage({
		"summary" : message + " platser ändrade",
		"detail" : "",
		"severity" : "info"
	});
}

function handleSessionExpired() {
	var newUrl = location.protocol + '//' + location.host
			+ "/TransactionDemo/session-expired.jsf";
	window.location.assign(newUrl);
}

function handleChat(message) {
    var  widgetRefOutput = PF('textOutput');
    var textAreaOutput = $(widgetRefOutput.jqId)[0];
    if (textAreaOutput.value == "") {
    	textAreaOutput.value = message;
    } else {
   		textAreaOutput.value = textAreaOutput.value + "\n" + message;
    }	
    var  widgetRefInput = PF('textInput');
    var textAreaInput = $(widgetRefInput.jqId)[0];
    textAreaInput.value = "";
}


package com.cgi.osd.transactiondemo.presentation.gui;

import java.net.URL;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.persistence.PersistenceManagerBeanMockup;

/**
 * This class is responsible for testing the REST API.
 *
 */
@RunWith(Arquillian.class)
public class RestTest {

	@Deployment(testable = false)
	public static Archive<WebArchive> createTestArchive() {
		final WebArchive archive = ShrinkWrap.create(WebArchive.class, "RestTest.war");
		archive.addClasses(PersistenceManagerBeanMockup.class);
		archive.addPackages(true, "com.cgi.osd.transactiondemo.logic");
		archive.addPackages(true, "com.cgi.osd.transactiondemo.persistence");
		archive.addPackages(true, "com.cgi.osd.transactiondemo.util");
		archive.addPackages(true, "com.cgi.osd.transactiondemo.presentation.rest");
		archive.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml");
		archive.addAsWebInfResource("test-ds.xml");
		archive.addAsWebInfResource("test-beans.xml", "beans.xml");

		return archive;
	}

	@ArquillianResource
	private URL baseURI;

	@Test
	@RunAsClient
	public void testFreeSeats() {
		final Client client = ClientBuilder.newClient();
		final String webPath = this.baseURI.toString() + "rest/reservation/free-seats";
		final WebTarget webTarget = client.target(webPath);
		final Response webResponse = webTarget.request(MediaType.APPLICATION_JSON).get();
		Assert.assertEquals(200, webResponse.getStatus());
		final GenericType<List<SeatDO>> receiver = new GenericType<List<SeatDO>>() {
		};

		final List<SeatDO> freeSeats = webResponse.readEntity(receiver);
		Assert.assertEquals(10, freeSeats.size());
		client.close();

	}

}

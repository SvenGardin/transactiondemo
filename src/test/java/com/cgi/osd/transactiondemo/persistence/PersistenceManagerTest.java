
package com.cgi.osd.transactiondemo.persistence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.cgi.osd.transactiondemo.logic.domainobject.BookingDO;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.LockStrategy;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.SeatBookedException;

/**
 * This class is responsible for testing the persistence manager using no mockup.
 *
 */
@RunWith(Arquillian.class)
public class PersistenceManagerTest {

	@Deployment
	public static Archive<WebArchive> createTestArchive() {
		final WebArchive archive = ShrinkWrap.create(WebArchive.class, "PersistenceManagerTest.war");
		archive.addPackages(true, "com.cgi.osd.transactiondemo.logic");
		archive.addPackages(true, "com.cgi.osd.transactiondemo.persistence");
		archive.addPackages(true, "com.cgi.osd.transactiondemo.util");
		archive.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml");
		archive.addAsWebInfResource("test-ds.xml");
		archive.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");

		return archive;
	}

	@Inject
	private PersistenceManager persistenceManager;

	@PersistenceContext(unitName = "primary")
	private EntityManager em;

	@Inject
	UserTransaction utx;

	@Inject
	LockTracker lockTracker;

	private final int numberOfCustomers = 5;
	private final int numberOfSeats = 10;

	@Before
	public void initDataBase() {
		try {
			this.utx.begin();
			this.em.joinTransaction();
			clearTables();
			initCustomer();
			initSeat();
			this.utx.commit();
		} catch (final NotSupportedException e) {
			fail("NotSupportedException: " + e.getMessage());
		} catch (final SystemException e) {
			fail("SystemException: " + e.getMessage());
		} catch (final SecurityException e) {
			fail("SecurityException: " + e.getMessage());
		} catch (final IllegalStateException e) {
			fail("IllegalStateException: " + e.getMessage());
		} catch (final RollbackException e) {
			fail("RollbackException: " + e.getMessage());
		} catch (final HeuristicMixedException e) {
			fail("HeuristicMixedException: " + e.getMessage());
		} catch (final HeuristicRollbackException e) {
			fail("HeuristicRollbackException: " + e.getMessage());
		}
	}

	/**
	 * This method tests creation of one booking and the selection of a the customer's bookings using an existing
	 * customer.
	 */
	@Test
	public void createBookingExistingCustomerTest() {
		try {
			final String email = "sven1.gardin1@cgi.com";
			final CustomerDO customer = this.persistenceManager.getCustomer(email);
			final List<SeatDO> freeSeats = this.persistenceManager.getFreeSeats();
			final List<SeatDO> seatsToBook = new LinkedList<>();
			final SeatDO seatToBook = freeSeats.get(0);
			this.persistenceManager.setLockStrategy(LockStrategy.OPTIMISTIC);
			this.persistenceManager.lockSeat(seatToBook, customer, true);
			seatToBook.setPessimisticLocked(true);
			seatsToBook.add(seatToBook);
			this.persistenceManager.createBookings(customer, seatsToBook);

			final List<SeatDO> customerSeats = this.persistenceManager.getCustomerSeats(customer);
			final SeatDO customerSeat = customerSeats.get(0);
			assertEquals(true, customerSeat.isBooked());

			final Set<BookingDO> bookingSet = this.persistenceManager.getCustomersBookings(customer);
			assertNotEquals("No booking found for customer", 0, bookingSet.size());

			final BookingDO bookings[] = bookingSet.toArray(new BookingDO[1]);
			assertEquals(1, bookings.length);

			final BookingDO booking = bookings[0];
			final Integer bookedSeatNumber = new Integer(seatToBook.getSeatNumber());
			assertTrue(booking.getSeatNumbers().contains(bookedSeatNumber));
		} catch (final SeatBookedException e) {
			fail("Got a SeatBookedException: " + e.getMessage());
		} catch (final SecurityException e) {
			fail("Got a SecurityException: " + e.getMessage());
		} catch (final IllegalStateException e) {
			fail("Got a IllegalStateException: " + e.getMessage());
		}
	}

	/**
	 * This method tests creation of several bookings and the selection of a the customer's bookings using an new
	 * customer.
	 */
	@Test
	public void createBookingNewCustomerTest() {
		try {
			final List<SeatDO> freeSeats = this.persistenceManager.getFreeSeats();
			final List<SeatDO> bookedSeats = new LinkedList<>();
			final String email = "sven.gardin@cgi.com";
			final CustomerDO customer = new CustomerDO("Sven", "Gardin", email, "0123456789");
			this.persistenceManager.setLockStrategy(LockStrategy.OPTIMISTIC);

			for (int i = 0; i < this.numberOfSeats / 2; i++) {
				final SeatDO bookedSeat = freeSeats.get(i);
				this.persistenceManager.lockSeat(bookedSeat, customer, true);
				bookedSeats.add(bookedSeat);
			}

			this.persistenceManager.createBookings(customer, bookedSeats);
			final List<SeatDO> customerSeats = this.persistenceManager.getCustomerSeats(customer);

			for (int i = 0; i < this.numberOfSeats / 2; i++) {
				final SeatDO customerSeat = customerSeats.get(i);
				assertEquals(true, customerSeat.isBooked());
				assertTrue(!customerSeat.getBookingReference().isEmpty());
			}
			for (int i = this.numberOfSeats / 2; i < customerSeats.size(); i++) {
				final SeatDO customerSeat = customerSeats.get(i);
				assertEquals(false, customerSeat.isBooked());
				assertTrue(customerSeat.getBookingReference().isEmpty());
			}

			final Set<BookingDO> bookingSet = this.persistenceManager.getCustomersBookings(customer);
			final BookingDO bookings[] = bookingSet.toArray(new BookingDO[1]);
			assertEquals(1, bookings.length);
			final BookingDO booking = bookings[0];
			assertEquals(this.numberOfSeats / 2, booking.getSeatNumbers().size());

		} catch (final SeatBookedException e) {
			fail("Got a SeatBookedException: " + e.getMessage());
		}
	}

	/**
	 * This method tests if customer domain objects can be retrieved from the database.
	 */
	@Test
	public void customerPersistenceTest() {
		for (int i = 0; i < this.numberOfCustomers; i++) {
			final String email = "sven" + i + ".gardin" + i + "@cgi.com";
			final String firstName = "Sven" + i;
			final String lastName = "Gardin" + i;
			final String telephoneNumer = "666" + i;

			final CustomerDO customerFetched = this.persistenceManager.getCustomer(email);
			assertEquals(email, customerFetched.getEmail());
			assertEquals(firstName, customerFetched.getFirstName());
			assertEquals(lastName, customerFetched.getLastName());
			assertEquals(telephoneNumer, customerFetched.getTelephoneNumber());
		}
	}

	/**
	 * This method verifies that the injection of instance variables is performed properly.
	 */
	@Test
	public void injectTest() {
		assertNotNull(this.em);
		assertNotNull(this.persistenceManager);
		assertNotNull(this.utx);
	}

	/**
	 * This method verifies that a SeatBooked exception is thrown when a customer tries to book an already booked seat.
	 * Note that the test is using nothing else but traditional try catch handling. This seems to be the most suitable
	 * way to design a test in Arquillian.
	 */
	@Test
	public void optimisticExceptionTest() {
		final String email = "sven1.gardin1@cgi.com";
		final CustomerDO customer1 = this.persistenceManager.getCustomer(email);
		final List<SeatDO> freeSeats = this.persistenceManager.getFreeSeats();
		final List<SeatDO> seatsToBook = new LinkedList<>();
		try {
			final SeatDO seatToBook = freeSeats.get(0);
			this.persistenceManager.setLockStrategy(LockStrategy.OPTIMISTIC);
			this.persistenceManager.lockSeat(seatToBook, customer1, true);
			seatsToBook.add(seatToBook);

			// Make a booking from other customer
			final CustomerDO customer2 = this.persistenceManager.getCustomer("sven2.gardin2@cgi.com");
			this.persistenceManager.lockSeat(seatToBook, customer2, true);
			this.persistenceManager.createBookings(customer2, seatsToBook);
		} catch (final SecurityException e) {
			fail("Got a SecurityException: " + e.getMessage());
		} catch (final Exception e) {
			fail("Got un unexpected exception of type " + e.getClass().getName());
		}

		// Try to book the seat now booked by other customer
		try {
			this.persistenceManager.createBookings(customer1, seatsToBook);
			fail("Got no SeatBookedException.");
		} catch (final SeatBookedException e) {
			final String message = e.getMessage();
			assertEquals("Seat already booked.", message);
		}
	}

	private void clearTables() {
		final Query deleteOptimisticLockQuery = this.em.createQuery("DELETE FROM OptimisticLock");
		deleteOptimisticLockQuery.executeUpdate();

		final Query deleteSeatQuery = this.em.createQuery("DELETE FROM Seat");
		deleteSeatQuery.executeUpdate();

		final Query deleteBookingQuery = this.em.createQuery("DELETE FROM Booking");
		deleteBookingQuery.executeUpdate();

		final Query deleteCustomerQuery = this.em.createQuery("DELETE FROM Customer");
		deleteCustomerQuery.executeUpdate();
	}

	private void initCustomer() {

		final Query insertQuery = this.em.createNativeQuery(
				"INSERT INTO customer (create_date, update_date, version, first_name, last_name, telephone_number, email) "
						+ "VALUES(NOW(), NOW(), 0, :firstName, :lastName, :telephoneNumber, :email);");

		for (int i = 0; i < this.numberOfCustomers; i++) {
			insertQuery.setParameter("firstName", "Sven" + i);
			insertQuery.setParameter("lastName", "Gardin" + i);
			insertQuery.setParameter("telephoneNumber", "666" + i);
			insertQuery.setParameter("email", "sven" + i + ".gardin" + i + "@cgi.com");
			insertQuery.executeUpdate();
		}
	}

	private void initSeat() {
		final Query insertQuery = this.em.createNativeQuery(
				"INSERT INTO seat (create_date, update_date, version, seat_number, locked, lock_time, locked_by) "
						+ "VALUES(NOW(), NOW(), 0, :seatNumber, 0, null, null);");

		for (int i = 0; i < this.numberOfSeats; i++) {
			insertQuery.setParameter("seatNumber", i);
			insertQuery.executeUpdate();
		}
	}

}

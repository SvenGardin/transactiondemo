package com.cgi.osd.transactiondemo.persistence;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;

import com.cgi.osd.transactiondemo.logic.domainobject.BookingDO;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.LockStrategy;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.SeatBookedException;

/**
 * This class is responsible for constituting a test implementation of the PersistenceManager interface. It is used for
 * testing conversion between entity classes and domain object classes.
 *
 */
@Alternative
@Stateless
public class PersistenceManagerBeanMockup implements PersistenceManager {

	@Inject
	private PersistenceObjectFactory objectFactory;

	/*
	 * (non-Javadoc)
	 *
	 * @see osd.cgi.com.TransactionDemo.persistence.PersistenceFacade#createBookings(osd.cgi.com.TransactionDemo.logic.
	 * CustomerDO, java.util.Set)
	 */
	@Override
	public void createBookings(CustomerDO customerDO, Collection<SeatDO> seatDOs) throws SeatBookedException {
		throw new RuntimeException("Method not implemented in mockup.");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see osd.cgi.com.TransactionDemo.persistence.PersistenceFacade#getCustomer(java.lang.String)
	 */
	@Override
	public CustomerDO getCustomer(String email) {
		final Customer customer = new Customer(email, "Sven", "Gardin", "123456789");
		final CustomerDO customerDO = this.objectFactory.createCustomerDO(customer);
		return customerDO;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.cgi.osd.transactiondemo.persistence.PersistenceFacade#getCustomersBookings(com.cgi.osd.transactiondemo.logic.
	 * domainobject.CustomerDO )
	 */
	@Override
	public Set<BookingDO> getCustomersBookings(CustomerDO customer) {
		throw new RuntimeException("Not implemented");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * osd.cgi.com.TransactionDemo.persistence.PersistenceFacade#getCustomerSeats(osd.cgi.com.TransactionDemo.logic.
	 * domainobject.CustomerDO)
	 */
	@Override
	public List<SeatDO> getCustomerSeats(CustomerDO customerDO) {
		final List<Seat> seatEntities = createSeats();
		final Booking bookingEntity = new Booking("aUniqueBookingReference");
		final Customer customerEntity = this.objectFactory.createCustomer(customerDO);
		bookingEntity.setCustomer(customerEntity);
		seatEntities.get(0).setBooking(bookingEntity);
		final List<SeatDO> result = this.objectFactory.createSeatDOList(seatEntities);
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see osd.cgi.com.TransactionDemo.persistence.PersistenceFacade#getFreeSeats()
	 */
	@Override
	public List<SeatDO> getFreeSeats() {
		final List<Seat> seatEntities = createSeats();
		final List<SeatDO> result = this.objectFactory.createSeatDOList(seatEntities);
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.cgi.osd.transactiondemo.persistence.PersistenceFacade#getLockStrategy()
	 */
	@Override
	public LockStrategy getLockStrategy() {
		throw new RuntimeException("Method not implemented in mockup.");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.cgi.osd.transactiondemo.persistence.PersistenceFacade#lockSeat(com.cgi.osd.transactiondemo.logic.domainobject
	 * .SeatDO)
	 */
	@Override
	public void lockSeat(SeatDO seat, CustomerDO customer, boolean lock) throws SeatBookedException {
		throw new RuntimeException("Method not implemented in mockup.");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.cgi.osd.transactiondemo.persistence.PersistenceFacade#releaseOldSeatLocks(int)
	 */
	@Override
	public int releaseOldSeatLocks(int maxLockMinutes) {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.cgi.osd.transactiondemo.persistence.PersistenceFacade#releaseSeats(com.cgi.osd.transactiondemo.logic.
	 * domainobject.CustomerDO)
	 */
	@Override
	public void releaseSeats(CustomerDO customerDO) throws SeatBookedException {
		throw new RuntimeException("Method not implemented in mockup.");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.cgi.osd.transactiondemo.persistence.PersistenceFacade#setLockStrategy(com.cgi.osd.transactiondemo.persistence
	 * .LockTrackerBean .LockStrategy)
	 */
	@Override
	public void setLockStrategy(LockStrategy lockStrategy) {
		throw new RuntimeException("Method not implemented in mockup.");
	}

	private List<Seat> createSeats() {
		final List<Seat> result = new LinkedList<>();
		for (int i = 0; i < 10; i++) {
			final Seat seat = new Seat(i);
			result.add(seat);
		}
		return result;
	}

}

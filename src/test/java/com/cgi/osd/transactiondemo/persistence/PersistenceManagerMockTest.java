
package com.cgi.osd.transactiondemo.persistence;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;

/**
 * This class is responsible for testing the persistence manager using no mockup.
 *
 */
@RunWith(Arquillian.class)
public class PersistenceManagerMockTest {

	@Deployment
	public static Archive<WebArchive> createTestArchive() {
		final WebArchive archive = ShrinkWrap.create(WebArchive.class, "PersistenceManagerMockTest.war");
		archive.addClasses(PersistenceManagerBeanMockup.class);
		archive.addPackages(true, "com.cgi.osd.transactiondemo.logic");
		archive.addPackages(true, "com.cgi.osd.transactiondemo.persistence");
		archive.addPackages(true, "com.cgi.osd.transactiondemo.util");
		archive.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml");
		archive.addAsWebInfResource("test-ds.xml");
		archive.addAsWebInfResource("test-beans.xml", "beans.xml");

		return archive;
	}

	@Inject
	private PersistenceManager persistenceManager;

	@Test
	public void testMockFreeSeats() {
		final List<SeatDO> seats = this.persistenceManager.getFreeSeats();
		assertEquals(10, seats.size());
		for (int i = 0; i < seats.size(); i++) {
			final SeatDO seat = seats.get(i);
			assertEquals(i, seat.getSeatNumber());
			assertEquals(false, seat.isBooked());
			assertEquals("", seat.getBookingReference());
		}
	}

}

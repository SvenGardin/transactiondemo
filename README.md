TransactionDemo: Assortment of technologies including Arquillian
========================
Author: Henrik Andersson
Level: Intermediate
Technologies: CDI, JSF, JPA, EJB, JPA, 
Summary: A simple booking application
Target Project: WildFly 10


System requirements
-------------------

All you need to build this project is Java 8 (Java SDK 1.8) or better, Maven 3.1 or better.

The application this project produces is designed to be run on JBoss WildFly 10 and MySQL 5.6.

 
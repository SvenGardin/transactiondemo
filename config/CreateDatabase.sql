BEGIN;

CREATE DATABASE IF NOT EXISTS `demo_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci;
USE `demo_db`;

DROP TABLE IF EXISTS `optimistic_lock`;
DROP TABLE IF EXISTS `seat`;
DROP TABLE IF EXISTS `booking`;
DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `telephone_number` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  PRIMARY KEY (`p_key`),
  UNIQUE KEY `UNIQUE_EMAIL` (`email`),
  KEY `INDEX_CUSTOMER_NAME` (`first_name`,`last_name`),
  KEY `INDEX_EMAIL` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

CREATE TABLE `booking` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` int(11) NOT NULL,
  `p_key_customer` int(11) NOT NULL,
  `booking_reference` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  PRIMARY KEY (`p_key`),
  UNIQUE KEY `UNIQUE_BOOKING_REFERENC` (`booking_reference`),
  KEY `INDEX_BOOKING_REFERENCE` (`booking_reference`),
  KEY `INDEX_BOOKING_CUSTOMER` (`p_key_customer`),
  CONSTRAINT `FK_BOOKING_CUSTOMER` FOREIGN KEY (`p_key_customer`) REFERENCES `customer` (`p_key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

CREATE TABLE `seat` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` int(11) NOT NULL,
  `p_key_booking`int(11),
  `seat_number` int(11) NOT NULL,
  `locked` boolean NOT NULL DEFAULT false,
  `lock_time` datetime,
  `locked_by` varchar(255) COLLATE utf8_swedish_ci,
  PRIMARY KEY (`p_key`),
  UNIQUE KEY `UNIQUE_SEAT_NUMBER` (`seat_number`),
  KEY `INDEX_BOOKED` (`p_key_booking`),
  KEY `INDEX_SEAT` (`seat_number`),
  KEY `INDEX_LOCKED` (`locked`), 
  KEY `INDEX_LOCKED_BY` (`locked_by`), 
  CONSTRAINT `FK_SEAT_BOOKING` FOREIGN KEY (`p_key_booking`) REFERENCES `booking` (`p_key`) ON DELETE CASCADE ON UPDATE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

CREATE TABLE `optimistic_lock` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` int(11) NOT NULL,
  `p_key_customer` int(11) NOT NULL,
  `p_key_seat` int(11) NOT NULL,
  `seat_version` int(11) NOT NULL,
  `lock_time` datetime,
  PRIMARY KEY (`p_key`),
  KEY `INDEX_CUSTOMER` (`p_key_customer`),
  KEY `INDEX_SEAT` (`p_key_seat`),
  KEY `INDEX_LOCK_TIME` (`lock_time`),
  UNIQUE KEY `UNIQUE_OPTIMISTIC_LOCK` (`p_key_customer`, `p_key_seat`),
  CONSTRAINT `FK_OPTIMISTIC_LOCK_CUSTOMER` FOREIGN KEY (`p_key_customer`) REFERENCES `customer` (`p_key`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_OPTIMISTIC_LOCK_SEAT` FOREIGN KEY (`p_key_seat`) REFERENCES `seat` (`p_key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

DROP PROCEDURE IF EXISTS `init_seat`;

DELIMITER $$  
CREATE PROCEDURE init_seat()

BEGIN
	DECLARE seat_number INT DEFAULT 1 ;
	INIT_LOOP: LOOP         
		INSERT INTO `seat` (create_date, update_date, version, seat_number) VALUES(NOW(), NOW(), 0, seat_number);
		SET seat_number = seat_number + 1;
		IF seat_number = 101 THEN
			LEAVE INIT_LOOP;
		END IF;
	END LOOP INIT_LOOP;
END $$
DELIMITER ;


CALL `init_seat`;
DROP PROCEDURE `init_seat`;
COMMIT;